# JMorantes Test Developer
## Bank of my dreams


> knowledge test made with the best disposition and with the best attitude for the Ecosystems company

## Requeriments
- Angular cli v 11.0 +
- Node JS v 14.17LT
- happiness

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

This test has a pipeline and an auxiliary repository hosted in Azure Devops, these popliners allow us to automate the deployment, streamlining processes and maintaining continuous development integration.

>Link Azure Devops: https://dev.azure.com/JMorantes-Aplications/PersonalSoftwares
If permissions are required, ask the developer or email: morat26@gmail.com

## Installation

Dillinger requires [Node.js](https://nodejs.org/) v14+ to run.

Install the dependencies and devDependencies and start the server.

```sh
git clone https://gitlab.com/morat26/test-jmorantes.git
cd test-jmorantes
npm install
ng serve
open browser http://localhost:4200/login
```

## User Test 

This test is using mock data, therefore there is no implicit connection to a data provider service.

To start session there are the following users:

| Type User | Credentials |
| ------ | ------ |
| Test recommended | User : jmorantes,
|  | Password : 1234 |
| Test1 | User : test |
|  | Password : test1 |


## License

MIT

**Free Software, Hell Yeah!**
