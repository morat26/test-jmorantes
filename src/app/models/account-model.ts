export interface AccountModel {
    idAccount : number;
    idUser : number;
    type: string;
    accountName: string;
    status : string; 
    currency : string;
    balance : number;
    isSelect : boolean;
    img: string;
  }
  