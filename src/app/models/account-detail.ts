export interface AccountDetail {
    idDetail : number; 
    idAccount : number;
    fromData : string;
    description: string;
    currency: string;
    value : number; 
    balance : number;
    isSelect : boolean;
  }
  