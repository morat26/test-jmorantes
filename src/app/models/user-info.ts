import { Data } from '@angular/router';

export interface UserInfo {
    idUser : number;
    user: string;
    password: string;
    name : string; 
    img : string;
    lastLogin : Date;
  }
  