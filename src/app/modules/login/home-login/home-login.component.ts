import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthorizationService } from 'src/app/services/authorization-service';
import { ResultModel } from 'src/app/models/result.model';
import { Router } from '@angular/router';
import { DataTransferService } from 'src/app/services/data-transfer-service';
import { UserDto } from 'src/app/models/user-dto';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-home-login',
  templateUrl: './home-login.component.html',
  styleUrls: ['./home-login.component.scss']
})
export class HomeLoginComponent implements OnInit {

  public authorizationForm : FormGroup; 
  public CONTROL_USER : string = "userCtrl";  
  public CONTROL_PASSWOR : string = "passwordCtrl";

  constructor(
    private fromBuilder : FormBuilder,
    private authorizationService : AuthorizationService,
    public dataTransferService : DataTransferService,
    private router: Router,
    private loading: LoadingService
  ) { 
    this.authorizationForm = this.buildFormGroupParents();
  }

  async ngOnInit()  { 
   await this.validationSessionUser();
   await this.validationTokenApp();
  }

  public async  validationTokenApp(){
     if(localStorage.getItem('access-token') == null){
      await this.authenticationApp();
     }

    
  }
  public async validationSessionUser(){
    if(localStorage.getItem('userinfo') != null){
      this.router.navigate(['/dashboard']);
    }
  }

  public async authenticationApp (){
      this.authorizationService.authenticationApp().subscribe(
      (res: ResultModel) => {
        if (res.isValid) {
          localStorage.setItem('access-token', res.data);
          this.dataTransferService.loading = false;
        }
      }
    )
  }

  public buildFormGroupParents() : FormGroup{
    return  this.fromBuilder.group({
        userCtrl : [null, Validators.required],
        passwordCtrl : [null, Validators.required],
      });
  }

  public getControl(field:string): FormControl{
    return this.authorizationForm.get(field) as FormControl;
  } 

  public authorizationUser(): void {

    if(this.authorizationForm?.valid){
      let params : UserDto = { 
        user : this.authorizationForm.get(this.CONTROL_USER)?.value,
        password : this.authorizationForm.get(this.CONTROL_PASSWOR)?.value,
      };

      this.authorizationService.authorizationUser(params).subscribe(
        (res : ResultModel)=>{
          if(!res.isValid){
            this.authorizationForm.get(this.CONTROL_PASSWOR)?.setValue('')
            return;
          }
          localStorage.setItem('userinfo', JSON.stringify(res.data));
          this.dataTransferService.infoUser = res.data;
          this.router.navigate(['/dashboard']);

        }
      )
    };
  }

  public isNotFieldValid(field: string): boolean | undefined {
    return this.authorizationForm.get(field)?.invalid && this.authorizationForm.get(field)?.touched;
  }

  public isFiedValid(field: string): boolean | undefined{
    return this.authorizationForm.get(field)?.valid && this.authorizationForm.get(field)?.value.length >= 3;
  }
  
  displayFieldCss(field: string) : object {
    return {
      'is-invalid': this.isNotFieldValid(field),
      'is-valid': this.isFiedValid(field)
    };
  }

}
