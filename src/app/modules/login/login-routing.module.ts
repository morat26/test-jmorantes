import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeLoginComponent } from './home-login/home-login.component';

const routes: Routes = [
  {
    path : '',
    component : HomeLoginComponent,
    children :[
        {
            path: 'login',
            children : [{
                path : '',
                loadChildren : () => import('./home-login/home-login.component').then( m => m.HomeLoginComponent)
            }]
        },
        {
          path: 'createAcount',
          children : [{
              path : '',
              loadChildren : () => import('./create-account/create-account.component').then( m => m.CreateAccountComponent)
          }]
      },
        {
            path : '',
            redirectTo : '/login',
            pathMatch: 'full'
        }
    ] 
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class LoginRoutingModule { }
