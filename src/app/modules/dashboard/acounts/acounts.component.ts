import { Component, OnInit } from '@angular/core';
import { DataTransferService } from 'src/app/services/data-transfer-service';
import { BankService } from 'src/app/services/ bank-services-service';
import { ResultModel } from 'src/app/models/result.model';
import { AccountModel } from 'src/app/models/account-model';
import { ParseError } from '@angular/compiler';

@Component({
  selector: 'app-acounts',
  templateUrl: './acounts.component.html',
  styleUrls: ['./acounts.component.scss']
})
export class AcountsComponent implements OnInit {

  public accountsUser : any; 

  constructor(
    public dataTransferService : DataTransferService,
    public bankService : BankService
  ) { }

  async ngOnInit() {
    await this.validationDataUser();
    await this.getAccontsUser();
  }

  public async validationDataUser(){
    this.dataTransferService.validationDataUser();
  }

  public async getAccontsUser(){
    if(localStorage.getItem('Acounts') != null){
      var acounts =  JSON.parse(localStorage.getItem('Acounts')!); 
      this.calculateBalance(acounts);
      return;
    };

    let params = {idUser : this.dataTransferService.infoUser.idUser}
    this.bankService.getAccountsUser(params).subscribe(
      (res : ResultModel) =>{
        localStorage.setItem('Acounts', JSON.stringify(res.data));
        this.calculateBalance(res.data);
      }
    )
  }

  public calculateBalance(acounts :Array<AccountModel>) : void {
    let totalBalance = 0;
    this.dataTransferService.userAcounts = acounts;
    acounts.forEach((element : AccountModel)  => {
      totalBalance += Number(element.balance); 
    });
    this.dataTransferService.totalBalance = totalBalance.toString();
  }

  public showDetailAccount(item : AccountModel){
    this.dataTransferService.userAcounts.forEach((element:AccountModel)  => { element.isSelect = false; });
    item.isSelect = true;
    this.dataTransferService.acountSelect = item; 
    this.dataTransferService.showDetail = true;
  }

}
