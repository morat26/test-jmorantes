import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { HomeComponent } from './home/home.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NavBarComponent } from 'src/app/components/shared/nav-bar/nav-bar.component';
import { SidebarComponent } from 'src/app/components/shared/sidebar/sidebar.component';
import { NavUserComponent } from './nav-user/nav-user.component';
import { AcountsComponent } from './acounts/acounts.component';
import { AcountsDetailComponent } from './acounts-detail/acounts-detail.component';
import { NewProductComponent } from './new-product/new-product.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    HomeComponent,
    NavBarComponent,
    SidebarComponent,
    NavUserComponent,
    AcountsComponent,
    AcountsDetailComponent,
    NewProductComponent
  ],
  imports: [ 
    CommonModule,
    DashboardRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ]
})

export class DashboardModule { }
