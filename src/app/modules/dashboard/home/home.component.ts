import { Component, OnInit, ElementRef } from '@angular/core';
import { DataTransferService } from 'src/app/services/data-transfer-service';
import { BankService } from 'src/app/services/ bank-services-service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    public dataTransferService : DataTransferService,
    public bankService : BankService,
    private elementRef: ElementRef
  ) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(){
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = '#ffffff';
  }

}
