import { Component, OnInit, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AccountModel } from 'src/app/models/account-model';
import { DataTransferService } from 'src/app/services/data-transfer-service';

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.scss']
})
export class NewProductComponent implements OnInit {

  public newProductForm : FormGroup; 
  public showSuccess : boolean = false;

  constructor(
    private elementRef: ElementRef,
    private fromBuilder : FormBuilder,
    private router: Router,
    private dataTransferService : DataTransferService
    ) { 
      this.newProductForm = this.buildFormGroupParents();
    }

  ngOnInit(): void {
  }

  ngAfterViewInit(){
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = '#022c5e';
  }

  public buildFormGroupParents() : FormGroup{
    return  this.fromBuilder.group({
        productCtrl : [null, Validators.required],
        cellPhoneCtrl : [null, Validators.required],
        monthlyIncomeCtrl : [null, Validators.required],
      });
  }

  public isNotFieldValid(field: string): boolean | undefined {
    return this.newProductForm.get(field)?.invalid && this.newProductForm.get(field)?.touched;
  }

  public isFiedValid(field: string): boolean | undefined{
    return this.newProductForm.get(field)?.valid && this.newProductForm.get(field)?.value.length >= 3;
  }

  public displayFieldCss(field: string) : object {
    return {
      'is-invalid': this.isNotFieldValid(field),
      'is-valid': this.isFiedValid(field)
    };
  }

  public createNewProduct(){
    if(this.newProductForm?.valid){
      let newProduct : AccountModel = {
        idAccount : Math.random(),
        idUser : this.dataTransferService.infoUser.idUser,
        type: this.newProductForm.get('productCtrl')?.value,
        accountName: this.newProductForm.get('cellPhoneCtrl')?.value,
        status : 'Pending', 
        currency : 'USD',
        balance : this.newProductForm.get('monthlyIncomeCtrl')?.value,
        isSelect : false,
        img  :'icon-card.png'
      }
      var acounts : Array<AccountModel> = JSON.parse(localStorage.getItem('Acounts')!); 
      acounts.push(newProduct);
      localStorage.setItem('Acounts', JSON.stringify(acounts));
      this.showSuccess = true;
    }
  }

  public goToHome(){
    this.showSuccess = false;
    this.router.navigate(['/dashboard']);
  }

  


}
