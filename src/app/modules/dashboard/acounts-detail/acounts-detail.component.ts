import { Component, OnInit, Input } from '@angular/core';
import { DataTransferService } from 'src/app/services/data-transfer-service';
import { BankService } from 'src/app/services/ bank-services-service';
import { ResultModel } from 'src/app/models/result.model';
import { AccountModel } from 'src/app/models/account-model';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { AccountDetail } from 'src/app/models/account-detail';

@Component({
  selector: 'app-acounts-detail',
  templateUrl: './acounts-detail.component.html',
  styleUrls: ['./acounts-detail.component.scss']
})
export class AcountsDetailComponent implements OnInit {

  public accountDetails : Array<AccountDetail> = [];
  public closeModal! : string;
  public selectAccountDetail! : AccountDetail;

  constructor(
    public dataTransferService : DataTransferService,
    public bankService : BankService,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
    this.getDetailAccount();
  }

  public async validationDataUser() {
    this.dataTransferService.validationDataUser();
  }

  public getDetailAccount(): void{
    let params = {idAccount : this.dataTransferService.acountSelect.idAccount }
    this.bankService.getDetailsAccounts(params).subscribe(
      (res : ResultModel)=>{
        this.accountDetails = res.data;
      }
    )
  }

  public logoutAcountDetail(): void{
    this.dataTransferService.showDetail = false;
  }

  triggerModal(content : any, detailSelect : AccountDetail) {
     this.selectAccountDetail = detailSelect;

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((res) => {
      this.closeModal = `Closed with: ${res}`;
    }, (res) => {
      this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
