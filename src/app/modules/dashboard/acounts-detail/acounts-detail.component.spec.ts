import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AcountsDetailComponent } from './acounts-detail.component';

describe('AcountsDetailComponent', () => {
  let component: AcountsDetailComponent;
  let fixture: ComponentFixture<AcountsDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AcountsDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AcountsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
