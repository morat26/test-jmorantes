import { Component, OnInit } from '@angular/core';
import { DataTransferService } from 'src/app/services/data-transfer-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-user',
  templateUrl: './nav-user.component.html',
  styleUrls: ['./nav-user.component.scss']
})
export class NavUserComponent implements OnInit {

  constructor(
    public dataTransferService : DataTransferService,
    private router: Router
  ) { }

  ngOnInit(): void {
    if(this.dataTransferService.infoUser == null){
      let userInfo = localStorage.getItem('userinfo');
       this.dataTransferService.infoUser =  JSON.parse(userInfo!);
    }
  }

  public openNewProduct(){
    this.router.navigate(['/new-product']);
  }
}
