import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { UserInfo } from '../models/user-info';
import { USERS } from './data-moks';
import { ResultModel } from '../models/result.model';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { UserDto } from '../models/user-dto';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  private users : UserInfo[] = [];
 
  constructor(
    private http: HttpClient,
  ) { 
    this.users = USERS;
    
  }

  public  authenticationApp(): Observable<ResultModel> {
    let paramas = {
      appId : environment.app_key
    }
    const url = environment.url_api + 'Authentication';
    return  this.http.post<ResultModel>(url, paramas);
  }

  public authorizationUser(params: UserDto): Observable<ResultModel> {
    const url = environment.url_api + 'AuthorizationUser';
    return this.http.post<ResultModel>(url, params);
  }
  
}
