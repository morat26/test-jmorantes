import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ResultModel } from '../models/result.model';
import { AccountModel } from '../models/account-model';
import { AccountDetail } from '../models/account-detail';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BankService {

  private acount : AccountModel[] = [];
  private acountDetail : AccountDetail[] = [];
  

  constructor(
    private http: HttpClient,
  ) { 
  }

  public getAccountsUser(params: any): Observable<ResultModel> {
    const url = environment.url_api + 'UserAccount';
    return this.http.get<ResultModel>(url, {params: params});
  }

  public getDetailsAccounts(params: any): Observable<ResultModel> {
    const url = environment.url_api + 'UserAccountDetails';
    return this.http.get<ResultModel>(url, {params: params});
  }
  
}
