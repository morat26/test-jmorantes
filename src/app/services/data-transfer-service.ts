import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ResultModel } from '../models/result.model';
import { UserInfo } from '../models/user-info';
import { AccountModel } from '../models/account-model';
import { AccountDetail } from '../models/account-detail';


@Injectable({
  providedIn: 'root'
})
export class DataTransferService {

  public infoUser! : UserInfo ;
  public userAcounts : Array<AccountModel> = [];
  public detailAcount! : AccountDetail;
  public totalBalance : string = '0';
  public acountSelect : any;
  public showDetail : boolean = false;
  public loading : boolean = false;
  
  constructor(
  ) { 
  }

  public async validationDataUser(){
    if(this.infoUser == null){
      let userInfo = localStorage.getItem('userinfo');
       this.infoUser =  JSON.parse(userInfo!);
    }
  }

}
