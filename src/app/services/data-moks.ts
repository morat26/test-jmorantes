import { UserInfo } from '../models/user-info';
import { AccountModel } from '../models/account-model';
import { AccountDetail } from '../models/account-detail';

export const USERS: UserInfo[] = [
  {
    idUser : 1001,
    user  : 'jmorantes',
    password : '1234',
    name : 'Julian Morantes Rios',
    img : 'jmorantes.jpg',
    lastLogin : new Date
  },
  {
    idUser : 1002,
    user  : 'test',
    password : 'test1',
    name : 'I`m Groot',
    img : 'groot.jpeg',
    lastLogin : new Date
  }
];

export const ACCOUNT: AccountModel[] = [
  {
    idAccount : 1,
    idUser : 1001,
    type: 'Checking',
    accountName: '1234555**** - GOLD',
    status : 'Active', 
    currency : 'USD',
    balance : 8500000,
    isSelect : false,
    img  :'icon-card.png'
  },
  {
    idAccount : 2,
    idUser : 1002,
    type: 'Savings',
    accountName: '548833**** - CLASIC',
    status : 'Active', 
    currency : 'USD',
    balance : 5500000,
    isSelect : false,
    img  :'icon-card.png'
  },
  {
    idAccount: 2,
    idUser : 1001,
    type: 'Trading',
    accountName: '523333**** - Coins',
    status : 'Active', 
    currency : 'USD',
    balance : 15500000,
    isSelect : false,
    img  :'icon-save.png'
  },
  {
    idAccount: 3,
    idUser : 1001,
    type: 'Savings',
    accountName: '52312333**** - TEST',
    status : 'Active', 
    currency : 'USD',
    balance : 8000000,
    isSelect : false,
    img  :'icon-save.png'
  },
  {
    idAccount: 4,
    idUser : 1001,
    type: 'Trading',
    accountName: '51313400**** - FAIL',
    status : 'Desactivated', 
    currency : 'USD',
    balance : 0,
    isSelect : false,
    img  :'icon-card.png'
  },

];