import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpHeaders, HttpResponse } from "@angular/common/http";
import { Observable } from 'rxjs';
import { DataTransferService } from '../services/data-transfer-service';
import {catchError, map} from 'rxjs/operators'

@Injectable()
export class Interceptor implements HttpInterceptor {

  constructor(
    public dataTransferService : DataTransferService,
  ) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.dataTransferService.loading = true; 
    if(localStorage.getItem('access-token') != null) {
        const token =  localStorage.getItem('access-token');
        const headers = new HttpHeaders().set("Authorization", 'Bearer ' + token!); 
        const AuthRequest = request.clone( { headers: headers});
        return next.handle(AuthRequest).pipe(map(event => {
          if (event instanceof HttpResponse) {
             this.dataTransferService.loading = false;
          }         
          return event;
      }));
    } else {
       return next.handle(request);
    }
         

  }
}