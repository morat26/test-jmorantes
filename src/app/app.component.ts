import { Component, OnInit } from '@angular/core';
import { DataTransferService } from './services/data-transfer-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'test-jmorantes';
  loading: boolean = false;

  constructor(
    public dataTransferService : DataTransferService,
  ){}

  ngOnInit() {
  }

}
